#include <InfluxDb.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <Wire.h>
#include <Adafruit_BMP085.h>

#define INFLUXDB_HOST "192.168.1.69"
#define INFLUXDB_PORT 8086
#define INFLUXDB_DATABASE "my_sensors"

#define INTERVALL 60*1000

#define SERIAL_COMM

// Global variables
Influxdb influx(INFLUXDB_HOST, INFLUXDB_PORT);

Adafruit_BMP085 bmp;

ESP8266WiFiMulti WiFiMulti;
const char* ssid = "ssid";
const char* password = "password";


// Function headers
InfluxData readTemp(float value);
InfluxData rssiInflux(float value);


void setup() {
#ifdef SERIAL_COMM
  Serial.begin(115200);
  Serial.printf("Connecting to %s ", ssid);
#endif

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  WiFiMulti.addAP(ssid, password);
  while (WiFiMulti.run() != WL_CONNECTED) {
#ifdef SERIAL_COMM
    Serial.print(".");
#endif
    delay(100);
  }
#ifdef SERIAL_COMM
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
#endif


  // InfluxDB
  influx.setDb(INFLUXDB_DATABASE);

  // BMP180
  Wire.begin(D1, D3);
  //bmp180 stuff
  if (!bmp.begin()) {
#ifdef SERIAL_COMM
    Serial.println("No BMP180 / BMP085");
#endif
    //   while (1) {}
  }
}

void loop() {

  digitalWrite(LED_BUILTIN, HIGH);
  
  float rssi = WiFi.RSSI();
  float temp_c = bmp.readTemperature();
  InfluxData temp1 = readTemp(temp_c);
  influx.prepare(temp1);

  InfluxData rssi_influx = rssiInflux(rssi);
  influx.prepare(rssi_influx);

  influx.write();

  digitalWrite(LED_BUILTIN, LOW);

  delay(INTERVALL);

}


InfluxData rssiInflux(float value) {
  InfluxData rssi_value ("rssi");
  rssi_value.addTag("device", "esp8266");
  rssi_value.addTag("sensor", "esp");
  rssi_value.addValue("value", value);
  return rssi_value;
}


InfluxData readTemp(float value) {
  InfluxData temp_c ("temperature");
  temp_c.addTag("device", "switch_24");
  temp_c.addTag("sensor", "bmp180");
  temp_c.addValue("value", value);
  return temp_c;
}

